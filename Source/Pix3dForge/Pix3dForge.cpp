// Copyright Epic Games, Inc. All Rights Reserved.

#include "Pix3dForge.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Pix3dForge, "Pix3dForge" );
