// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Pix3dForgeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PIX3DFORGE_API APix3dForgeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
